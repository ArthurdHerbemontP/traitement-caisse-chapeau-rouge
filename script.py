import glob
import matplotlib.pyplot as plt
import pdb
import pandas as pd

def main():

  list = glob.glob("202*/**/Z001_*",recursive=True)

  CA_NET = []  
  CA   = {}
  k=0
  for i in list:
    path = i.split("\\")
    year = path[0]
    month = path[1]
    day = path[2][5:7]
    file = open(i)
    txt = file.read()
    # pdb.set_trace()
    pos_ca = txt.replace("\n",",").split('","').index('CA NET      ')
    CA_NET.append(float(txt.replace("\n",",").split('","')[pos_ca+2].strip("\"").strip(" ").replace(',',"")))
    k +=1
    CA[i] = {"CA":CA_NET[k-1],"year":year,"month":month,"day":day}
    
  return CA_NET,CA
  
def average(CA_NET):

  return sum(CA_NET)/len(CA_NET)

def CA_per_month(CA):
  
  CA_month = {}
  # pdb.set_trace()
  for key,value in CA.items():
    month_key = CA[key]["year"][2:]+'/'+CA[key]["month"]
    if month_key in CA_month:
      CA_month[month_key] += CA[key]["CA"]
    else:
      CA_month[month_key] = CA[key]["CA"]
  return CA_month

def CA_per_week(CA):
  CA_week = {}
  # pdb.set_trace()
  for key,value in CA.items():
    week_key = CA[key]["year"][2:]+'/'+ CA[key]["month"]+'/'+ str(int(int(CA[key]["day"])/7))
    if week_key in CA_week:
      CA_week[week_key] += CA[key]["CA"]
    else:
      CA_week[week_key] = CA[key]["CA"]
  return CA_week

def plot_CA_per_month(CA_dict_2_dim):
  lists = sorted(CA_dict_2_dim.items())
  x, y = zip(*lists)
  plt.plot(x,y)
  plt.suptitle('CA par mois')
  plt.ylabel('CA')
  plt.xlabel('Mois')
  plt.show()
  
def plot_CA_hist(CA_NET):
  plt.hist(CA_NET,20)
  plt.show()

def write_csv(csv_dict):
  
  df = pd.DataFrame(csv_dict.items())
  df.to_csv('CA_month.csv')